/**
 * Created by Vicnent on 12.05.2018.
 */

const DAYS = 0;
const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');
const ml = require('machine_learning');
const daysAnalyse = 14;
const daysProfit = 7;
const PROFABILITY = 100;

let url = "https://coinmarketcap.com/coins/views/all/";
let count = 0;
let timoutTime = 30000;

function getCoinData(coinName, coinUrl) {
    return new Promise((resolve) => {
        let isFinished = false;
        let isTimeout = false;

        setTimeout(function () {
            if (!isFinished) {
                isTimeout = true;
                let coinData = {
                    coinName: coinName,
                    coinUrl: coinUrl,
                    failed: true,
                    historicalData: []
                };

                count++;
                console.log("timeout " + coinName + " (" + count + ")");
                resolve(coinData);
            }
        }, timoutTime);

        request(coinUrl, function (error, response, body) {
            try {
                isFinished = true;
                let historicalData = [],
                    $ = cheerio.load(body, {decodeEntities: false});

                $("tr.text-right td:nth-child(5)").each(function () {
                    historicalData.push($(this).html());
                });

                let coinData = {
                    coinName: coinName,
                    coinUrl: coinUrl,
                    failed: false,
                    historicalData: historicalData
                };

                if (!isTimeout) {
                    count++;
                    console.log("done " + coinName + " (" + count + ")");
                    resolve(coinData);
                }
            } catch (ex) {
                isFinished = true;
                let coinData = {
                    coinName: coinName,
                    coinUrl: coinUrl,
                    failed: true,
                    historicalData: []
                };
                if (!isTimeout) {
                    count++;
                    console.log("failed " + coinName + " (" + count + ")");
                    resolve(coinData);
                }
            }
        });
    });
}

function getDateString(daysPast) {
    let dateOffset = (24 * 60 * 60 * 1000) * daysPast; // 5 days
    let myDate = new Date();
    myDate.setTime(myDate.getTime() - dateOffset);

    let dd = myDate.getDate();
    let mm = myDate.getMonth() + 1; //January is 0!
    let yyyy = myDate.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    return (yyyy.toString() + mm.toString() + dd.toString());
}

async function startMonitoring() {
    let fromDateString = getDateString(daysAnalyse);
    let toDateString = getDateString(0);

    let promises = [];

    // GET COINNAMES
    request(url, async function (error, response, body) {
        let $ = cheerio.load(body, {decodeEntities: false});

        // get url of historical data for each coin
        $(".currency-name-container").each(function (index) {
            let coinName = $(this).html();
            let coinUrl = `https://coinmarketcap.com${$(this).attr("href")}historical-data/?start=${fromDateString}&end=${toDateString}`;

            promises.push(getCoinData(coinName, coinUrl));
        });

        console.log("promises " + promises.length);

        let coinResults = [],
            failedData = 1;
        while (failedData > 0) {
            failedData = 0;
            await Promise.all(promises)
                .then((results) => {
                    count = 0;
                    promises = [];

                    for (let i = 0; i < results.length; i++) {
                        if (!results[i].failed) {
                            coinResults.push(results[i]);
                        } else {
                            promises.push(getCoinData(results[i].coinName, results[i].coinUrl));
                            failedData++;
                        }
                    }
                })
                .catch((e) => {
                    // Handle errors here
                    console.log("error");
                    console.error(e);
                });
        }

        let knn = await setupBot();
        testData(knn, coinResults);
    });
}

function testData(knn, testData) {
    console.log("test Data");

    for (let i = 0; i < testData.length; i++) {
        let prediction = knn.predict({
            x : testData[i].historicalData,
            k : 3,

            weightf : {type : 'gaussian', sigma : 10.0},
            distance : {type : 'euclidean'}
        });

        console.log("processing " + (i+1) + "/" + testData.length);

        if (prediction > 0.75) {
            // logs on server : /root/.pm2/logs
            console.log("predicted profit - result: " + testData[i].coinName);
        }
    }
}

async function setupBot() {
    console.log("setup bot");

    let coins = await getTrainingCoins();
    let coinResults = [];

    for (let c = 0; c < coins.length; c++) {
        let coinData = coins[c].historicalData;
        for (let i = 0; i < coinData.length; i++ ) {
            if (coinData.length > i + (daysAnalyse + daysProfit)) {
                let monthData = [],
                    startValue = coinData[i+daysAnalyse],
                    endValue = coinData[i+(daysAnalyse + daysProfit)],
                    growthPercentage = (endValue - startValue) / startValue * 100,
                    isProfitable = (growthPercentage > PROFABILITY) ? 1 : 0;

                for (let j = i; j < (i + daysAnalyse); j++ ) {
                    monthData.push(coinData[j]);
                }

                let coinMonthResults = {
                    data: monthData,
                    isProfitable: isProfitable,
                    growthPercentage: growthPercentage
                };

                coinResults.push(coinMonthResults);
            }
        }
    }
    let knn = await trainBot(coinResults);
    return (knn);
}

function getTrainingCoins() {
    return new Promise((resolve) => require('fs').readFile('coinData.json', 'utf8', function (err, data) {
        if (err) {

        } else {
            let obj = JSON.parse(data),
                result = [];

            for (let i = 0; i < obj.length; i++) {
                result.push(obj[i]);
            }

            resolve(result);
        }
    }));
}

function trainBot(trainingData) {
    let data = [];
    let result = [];

    for (let i = 0; i < trainingData.length; i++) {
        data.push(trainingData[i].data);
        result.push(trainingData[i].isProfitable);
    }

    console.log("train bot");

    return new ml.KNN({
        data : data,
        result : result
    });
}

startMonitoring();