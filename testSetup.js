/**
 * Created by Vicnent on 28.04.2018.
 */

const fs = require('fs');
const ml = require('machine_learning');
const daysAnalyse = 7;
const daysProfit = 5;
const PROFABILITY = 75;

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

function getTesData() {
    return new Promise((resolve) => require('fs').readFile('coinData.json', 'utf8', function (err, data) {
        if (err) {

        } else {
            let obj = JSON.parse(data),
                result = [];

            for (let i = 0; i < obj.length; i++) {
                result.push(obj[i]);
            }

            resolve(result);
        }
    }));
}

async function getProfitabilityData() {
    let coins = await getTesData(),
        coinResults = [];

    for (let c = 0; c < coins.length; c++) {
        let coinData = coins[c].historicalData;
        for (let i = 0; i < coinData.length; i++ ) {
            if (coinData.length > i + (daysAnalyse + daysProfit)) {
                let monthData = [],
                    startValue = coinData[i+daysAnalyse],
                    endValue = coinData[i+(daysAnalyse + daysProfit)],
                    growthPercentage = (endValue - startValue) / startValue * 100,
                    isProfitable = (growthPercentage > PROFABILITY) ? 1 : 0;

                for (let j = i; j < (i + daysAnalyse); j++ ) {
                    monthData.push(coinData[j]);
                }

                let coinMonthResults = {
                    data: monthData,
                    isProfitable: isProfitable,
                    growthPercentage: growthPercentage
                };

                coinResults.push(coinMonthResults);
            }
        }
    }


    let isProfitable = 0,
        isNotProfitable = 0;

    for (let i = 0; i < coinResults.length; i++ ) {
        if (coinResults[i].isProfitable === 1) {
            isProfitable++;
        } else {
            isNotProfitable++;
        }
    }

    console.log("found " + isProfitable + " profitable sets");
    console.log("found " + isNotProfitable + " not profitable sets");

    setupData(coinResults);
}

function setupData(coinResults) {
    coinResults = shuffle(coinResults);

    let cutArrayAt = Math.round((coinResults.length / 100) * 80),
        trainingData = [],
        testData = [];

    // devide into training data and test data
    for (let i = 0; i < coinResults.length; i++) {
        if (i < cutArrayAt) {
            trainingData.push(coinResults[i]);
        } else {
            testData.push(coinResults[i]);
        }
    }

    let knn = trainBot(trainingData);

    for (let i = 0; i < testData.length; i++) {
        let prediction = knn.predict({
            x : testData[i].data,
            k : 3,

            weightf : {type : 'gaussian', sigma : 10.0},
            distance : {type : 'euclidean'}
        });

        console.log("processing " + (i+1) + "/" + testData.length);

        if (prediction > 0.75) {
            console.log("predicted profit - result: " + testData[i].growthPercentage);
        }
    }
}

function trainBot(trainingData) {
    let data = [];
    let result = [];

    for (let i = 0; i < trainingData.length; i++) {
        data.push(trainingData[i].data);
        result.push(trainingData[i].isProfitable);
    }

    return new ml.KNN({
        data : data,
        result : result
    });
}

getProfitabilityData();
